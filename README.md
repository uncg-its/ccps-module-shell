# CCPS Framework - Module shell

This is an empty shell that you can use to make your own Module for a CCPS Framework app.

# Quick Instructions

1. Clone this repo, remove remotes and add your own new remote. DO NOT OVERWRITE THIS PACKAGE!
2. Edit `composer.json` to include your module's information. Make sure to edit the package name, its dependencies, its developer info, and MOST importantly, the namespacing data under psr-0 and the Laravel autoloader.
3. Rename `src/breadcrumbs/modulename.php` to the name of your module, and then edit the Breadcrumb definitions within.
4. Rename `src/routes/modulename.php` to the name of your module, and then edit the route definitions within.
5. Build your package per typical Laravel conventions
6. Edit the `src/ServiceProvider.php` file - namespace it with your package name.
7. If using built-in views or database migrations be sure to include them in `src/ServiceProvider.php`, per typical Laravel conventions
8. Create extra permissions around your module if needed, using a database seeder (seed the `ccps_permissions` table). You may or may not want to write other seeders to create roles (`ccps_roles`) and/or automatically apply these permissions to roles (`ccps_permission_role`) as well. *See below for details*
9. Be sure to provide a default configuration for your user, based on the template below.
10. Replace this README with your own information.

---

# Detailed Instructions

## Breadcrumbs and routes

Make sure you rename the file to match the keyed name of your package (e.g. whatever you will use in the configuration array, as shown below).

Instructions on how to use Breadcrumbs and Routes are included in each sample file in this repository - read appropriate documentation on the `davejamesmiller/breadcrumbs` package and Laravel routing if you are unsure after seeing the examples.

## Roles and Permissions

In keeping with CCPS Core practices, any data that should be inserted into the database with the installation of your package should be run as part of a *migration*. This can be done with the migration calling a seeder class (see below), or you can do direct DB work inside the migration. But the key here is to keep whatever seed data here separate from a "Laravel-style" seeder, which is intended more for test / fake data.

IF you want to make a Seeder class and call it in your migrations:

### Option 1 - implement Validated Seeder class

You will likely want to control access to your module's pages via the built-in CCPS Core ACL. This is simple to do, but you will need to make sure you add appropriate database seeds to accomplish this.

Inside this package are two example seeder classes - `ExamplePermissionSeeder` and `ExampleRoleSeeder` - that can be used to seed your permissions and roles. You will want to make sure you understand how these seeders utilize the `Uncgits\Ccps\Seeders\CcpsValidatedSeeder` class in the CCPS Core module when writing your seeders.

### Option 2 - roll your own

At minimum, you will want to add your new permissions to the `ccps_permissions` table - when you do this, you should make sure to populate the `source_package` field with the full name of your module's package (e.g. `uncgits/ccps-module-helloworld`). This will help in future updates to your package to identify the permissions that may need modifying.

Additionally, you can seed the `editable` field with a 0 to disallow editing of the permission in the ACL. This is generally a good idea so that your module is not breakable by the user.

The same will apply for Roles - seed the `ccps_roles` table, and be sure to populate the `source_package` field and, if desired, the `editable` field.

### Attaching Permissions to Roles

Finally, if you are creating a custom role as part of your module, you can write logic to assign your new permissions to the new role. These associations can be done with a simple `DB::insert()` statement on the `ccps_permission_role` pivot table. Just be sure you know what the effective IDs are for both your roles and permissions - they may vary from application to application.

The CCPS Core seeder that attaches Core permissions to the Administrator role is a decent starting point, but must be modified since we cannot make the same assumption that the Core package makes (the "administrator" role is ID 1, always, so we can skip the lookup/verification steps).

## Configuration array

This should be modified (info in CAPS) placed in `app/config/ccps.php`, inside of the `modules` array:

```
'MODULENAME' => [
    'package' => 'PACKAGENAME', // full name including prefix (e.g. uncgits/my-great-module)
    'icon' => 'ICON',
    'title' => 'TITLE',
    'index' => 'INDEX', // usually MODULENAME
    'parent' => 'PARENT', // usually 'admin'
    'required_permissions' => 'PERMISSIONS', // should match your seeded permissions, syntax according to Laratrust docs (can use 'mything-*' for wildcard matching); or use * to match anything (no restrictions).
    'use_custom_routes' => false,
    'custom_view_path' => false,
],
```

So, for instance:

```
'helloworld' => [
    'package' => 'uncgits/ccps-module-helloworld',
    'icon' => 'hand-paper-o',
    'title' => 'Hello World',
    'index' => 'helloworld',
    'parent' => 'admin',
    'required_permissions' => '*',
    'use_custom_routes' => false,
    'custom_view_path' => false,
],
```

# Version History

## 1.0.1

- Bugfix for `use` statement in Role seeder
- Flexible version constraints by default for Laravel and Core

## 1.0.0

- Initial release

# Questions?

[ccps-developers-l@uncg.edu](mailto:ccps-developers-l@uncg.edu)

[https://bitbucket.org/uncg-its/ccps-core](https://bitbucket.org/uncg-its/ccps-core)
