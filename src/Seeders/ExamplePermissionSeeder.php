<?php

namespace Uncgits\Ccps\PACKAGENAME\Seeders;

use App\CcpsCore\Permission;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class ExamplePermissionSeeder extends CcpsValidatedSeeder
{
    // Sample permissions seeding. The structure is checked against what is defined in the parent class ($permissionArrayConstruction)

    public $permissions = [
        [
            "name"         => "example-view",
            "display_name" => "Example - View",
            "description"  => "Example Module - view",
        ],
        [
            "name"         => "example-edit",
            "display_name" => "Example - Edit",
            "description"  => "Example Module - edit"
        ],
        [
            "name"         => "example-delete",
            "display_name" => "Example - Delete",
            "description"  => "Example Module - delete"
        ],
        [
            "name"         => "example-create",
            "display_name" => "Example - Create",
            "description"  => "Example Module - create"
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {

            // checks the data against the defined data structure.
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            // checks to be sure that data doesn't already exist in the table
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            // essential data that should be merged with each record before seeding
            $mergeData = [
                'source_package' => 'uncgits/PACKAGENAME',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 0
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
