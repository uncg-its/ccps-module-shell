<?php

namespace Uncgits\Ccps\MODULENAME;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register Artisan commands, define publishing, define route-loading or migration-loading... etc.

        // views
        // $this->loadViewsFrom(__DIR__ . '/views', 'MODULENAME'); // reference in app with view('MODULENAME::viewname')

        // migrations
        // $this->loadMigrationsFrom(__DIR__ . '/migrations/');
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
