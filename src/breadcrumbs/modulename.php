<?php

/*

Copy example below as a template, replace info in CAPS.
This will be pulled in automatically via CCPS module definitions

- Top level:

Breadcrumbs::register('NAME', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push("TITLE", route('ROUTENAME'));
});

- Children:

Breadcrumbs::register('NAME', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('PARENTNAME');
    $breadcrumbs->push("TITLE", route('ROUTENAME'));
});

*/